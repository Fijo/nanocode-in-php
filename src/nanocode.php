<?
class nanocode	{
	public $pkg;
	public $loaded;
	public $handling_class;
	private $ifs;
	private $ifsCount;
	private $namespaces;
	public $vars;

	function __construct( $pkg, $loaded = null )	{
		$this->pkg = $pkg;

		$this->loaded = isset($loaded) == true?
								$loaded
								: array(0, 0);

		$this->ifs = array();
		$this->ifsCount = 0;
		$namespaces = array();
		$vars = array();
	}

	public function reset ( $pkg, $loaded = null )	{
		$this->pkg = $pkg;

		isset($loaded) == true && ($this->loaded = $loaded);

		$this->ifs = array();
		$this->ifsCount = 0;
		$namespaces = array();
		$vars = array();
	}

	public function close ( )	{
		$loaded = $this->loaded;
		unset($this);
		return $loaded;
	}

	private function str_in ($s, $i, $u)	{
		$t = ($n = $s[ $i++ ]) != '\\'? $n: ($i != $u? $s[ $i++ ]: '');

		if($i < $u)	{
			do $t .= ($n = $s[ $i++ ]) != '\\'?
					$n
					: ($i != $u? $s[ $i++ ]: '');

			while($i != $u);
		}

		return $t;
	}

	/* execCode ( (array) $strCodeLines )
	**
	** $strCodeLines cannot be empty
	** else endless loop
	*/

	public function execCode ( $s )	{
		$l = count($s);
		$p = 0;

		do	{
			if(($v = $this->execFunc( $s[ $p++ ], $p )) != null)
				$p = $v;
		}
		while($p != $l);		
	}


	function execFunc ( $s, $q = null )	{
		$l = strlen($s);

		if(($p = strpos($s, ':')) === false) return null;

		$func = substr($s, 0, $p++);


		if($q != null)	{
			if($this->ifsCount != 0 &&
				$this->ifs[ $this->ifsCount -1 ] == false &&
				$func != 'else' &&
				$func != 'elseif' &&
				$func != 'butif' &&
				$func != 'fi')
				return null;

			else switch($func)	{
					case 'namespace':
						$this->namespaces[ substr($s, $p) ] = $q;
						return null;
						break;

					case 'gotofi':
						$this->ifsCount--;
						array_pop($this->ifs);

					case 'goto':
						if(($f = $this->namespaces[ substr($s, $p) ]) != null)
							return $f;

						else die('A namespace "'.substr($s, $p).'" does not exist');
						break;

					case 'gotofa':
						$this->ifsCount = 0;
						$this->ifs = array();

						if(($f = $this->namespaces[ substr($s, $p) ]) != null)
							return $f;

						else die('A namespace "'.substr($s, $p).'" does not exist');
						break;
					}
		}


		$iFunc = 0;
		$a = array();
		$b = $p;

		do switch($s[ $p++ ])	{
		case '\\':
#			for easier debuging
#			if(($p += 2) > $l +2) die('nanocode syntax error: Cannot end a command with a \\!');

			$p += 2;
			break;

		case ':':
			$iFunc++;
			break;

		case ',':
			if($iFunc == 0)
				$a[] = $this->str_in($s, $b, ($b = $p) -1);

			break;

		case '.':
			if($iFunc-- == 1) $a[] = $this->execFunc( substr($s, $b++, -$b + ($b = $p)) );
			break;

		case '_':
			if(($iFunc -= $cl = (int) substr( $s, $p, 2 )) == 1)	{
				$a[] = $this->execFunc( substr($s, $b++, -$b + ($b = $p)) );
				$b += 2;
			}
			$p += 2;
			break;
		}
		while($p < $l);

		$a[] = $iFunc != 0?
					$this->execFunc( substr($s, $b, $p - $b) )
					: $this->str_in( $s, $b, $p );

		if($q == null) return $this->funcs($func, $a);
		else $this->funcs($func, $a);
	}

	public function funcs ( $f, $a )	{
		switch($f)	{
		case 'int':
			return (int) $a[0];
			break;

		case 'float':
			return (float) $a[0];	
			break;

		case 'string':
			return (string) $a[0];	
			break;

		// operators

		case 'con':
			return $a[0] . $a[1];	
			break;

		case 'sum':
			return $a[0] + $a[1];	
			break;

		case 'prod':
			return $a[0] * $a[1];	
			break;

		case 'dif':
			return $a[0] - $a[1];	
			break;

		case 'divi':
			return $a[0] / $a[1];	
			break;

		case 'and':
			return $a[0] && $a[1];	
			break;

		case 'or':
			return $a[0] || $a[1];	
			break;

		case 'band':
			return $a[0] & $a[1];	
			break;

		case 'bor':
			return $a[0] | $a[1];	
			break;

		case 'xor':
			return $a[0] ^ $a[1];	
			break;

		case 'equal':
			return $a[0] == $a[1];	
			break;

		case 'nequal':
			return $a[0] != $a[1];	
			break;

		case 'eequal':
			return $a[0] === $a[1];	
			break;

		case 'enequal':
			return $a[0] !== $a[1];	
			break;

		case 'smaller':
			return $a[0] < $a[1];	
			break;

		case 'bigger':
			return $a[0] > $a[1];	
			break;

		case 'smallereq':
			return $a[0] <= $a[1];	
			break;

		case 'biggereq':
			return $a[0] >= $a[1];	
			break;



		// constants

		case 'true':
			return true;	
			break;

		case 'false':
			return false;	
			break;

		case 'null':
			return null;	
			break;



		// vars

		case 'set':
			return $this->vars[ $a[0] ] = $a[1];	
			break;

		case 'S':
			return $this->vars[ $a[0] ];	
			break;


		// ifs

		case 'if':
			$this->ifsCount++;
			$this->ifs[] = $a[0] == true;	
			break;

		case 'butif':
			return $this->ifs[ $this->ifsCount -1 ] = $a[0] == true;
			break;

		case 'else':
			$x = $this->ifsCount -1;

			return $this->ifs[ $x ] = $this->ifs[ $x ] == false;
			break;

		case 'elseif':
			$x = $this->ifsCount -1;

			return $this->ifs[ $x ] = $this->ifs[ $x ] == false
							? $a[0] == true
							: false;
			break;


		case 'fi':
			$this->ifsCount--;
			array_pop($this->ifs);
			break;


		// lang functions

		case 'import':
			switch($a[0])	{
			// add imports as in the following example
			//case 'test>example':
			//	 use another flag for each function
			//	 if you have used all flags you can additional use $this->loaded[1] and som on

			//	($this->loaded[0] & 1) != 1 &&
			//		(require './libs/example.php') &&
			//		($this->loaded[0] += 1);
			//	break;
			}
			break;




		// other funcs

		case 'echo':
			echo $a[0];	
			break;



		default:
			array_unshift( $a, $f );
			return c( $a );
		}
	}
}

// example:
// nanocode.execCode('set:hallo,int:3;set:hallo,dif:S:hallo.1;alert:sum:S:hallo.int:4');
// nanocode.execCode('set:hallo,3;set:hallo,dif:S:hallo.1;alert:sum:S:hallo.int:4;namespace:xy;set:hallo,prod:int:2.S:hallo;alert:sum:string:Der aktuelle Wert ist .S:hallo;goto:xy;');


?>
